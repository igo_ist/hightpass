const path = require(`path`);
const fs = require(`fs`);
const webpack = require(`webpack`);
const MiniCssExtractPlugin = require(`mini-css-extract-plugin`);
const HtmlWebpackPlugin = require(`html-webpack-plugin`);


const PATHS = {
  src: path.join(__dirname, `./src`),
  dist: path.join(__dirname, `./dist`),
  assets: `assets/`,
};

const PAGES_DIR = `${PATHS.src}/pug/pages/`;
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith(`.pug`));

module.exports = {
  mode: `development`,
  externals: {
    paths: PATHS
  },
  entry: {
    app: `${PATHS.src}/js`
  },
  output: {
    filename: `./[name].js`,
    path: PATHS.dist,
    publicPath: `/`
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: `vendors`,
          test: /node_modules/,
          chunks: `all`,
          enforce: true,

        }
      }
    }
  },
  module: {
    rules: [{
      test: /\.pug$/,
      oneOf: [
        // this applies to <template lang=`pug`> in Vue components
        {
          resourceQuery: /^\?vue/,
          use: [`pug-plain-loader`]
        },
        // this applies to pug imports inside JavaScript
        {
          use: [`pug-loader`]
        }
      ]
    }, {
      test: /\.js$/,
      loader: `babel-loader`,
      exclude: `/node_mudules/`
    }, {
      test: /\.scss$/,
      use: [
        `style-loader`,
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            esModule: false,
          },
        },
        {
          loader: `css-loader`,
          options: {
            sourceMap: true,
            url: false
          }
        }, {
          loader: `postcss-loader`,
          options: {
            sourceMap: true,
            postcssOptions: {
              config: true,
            },
          }
        }, {
          loader: `sass-loader`,
          options: {
            sourceMap: true,
          }
        }
      ]
    }, {
      test: /\.css$/,
      use: [
        `style-loader`,
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            esModule: false,
          },
        },
        {
          loader: `css-loader`,
          options: { sourceMap: true }
        }, {
          loader: `postcss-loader`,
          options: {
            sourceMap: true,
            postcssOptions: {
              config: true,
            },
          }
        }
      ],
    }]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: `./[name].css`,
    }),
    ...PAGES.map(page => new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/${page}`,
      filename: `./${page.replace(/\.pug/, `.html`)}`
    })),
    new webpack.SourceMapDevToolPlugin({
      filename: `[file].map`
    })
  ],
};