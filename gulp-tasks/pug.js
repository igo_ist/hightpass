"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import pug from "gulp-pug";
import gulpif from "gulp-if";
import replace from "gulp-replace";
import webpack from "webpack-stream";
import browsersync from "browser-sync";
import yargs from "yargs";


import HtmlWebpackPlugin from 'html-webpack-plugin'



const argv = yargs.argv,
    production = !!argv.production;

gulp.task("pug", () => {
    return gulp.src(paths.pug.src)
        .pipe(pug({
            pretty: true
        }))
        .pipe(webpack({
            mode: 'development',
            plugins: [
                new HtmlWebpackPlugin()
            ]
        }))
        .pipe(gulp.dest(paths.pug.dist))
        .pipe(browsersync.stream());
});