"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import responsive from "gulp-responsive";
import imagemin from "gulp-imagemin";
import imageminPngquant from "imagemin-pngquant";
import imageminZopfli from "imagemin-zopfli";
import imageminMozjpeg from "imagemin-mozjpeg";
import imageminGiflossy from "imagemin-giflossy";
import newer from "gulp-newer";
import debug from "gulp-debug";
import browsersync from "browser-sync";
import yargs from "yargs";

const argv = yargs.argv,
  production = !!argv.production;

gulp.task(`images`, () => {
  return gulp.src(paths.images.src)
    .pipe(newer(paths.images.dist))
    .pipe(gulpif(production, imagemin([
      imageminGiflossy({
        optimizationLevel: 3,
        optimize: 3,
        lossy: 2
      }),
      imageminPngquant({
        speed: 5,
        quality: [0.6, 0.8]
      }),
      imageminZopfli({
        more: true
      }),
      imageminMozjpeg({
        progressive: true,
        quality: 90
      }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { removeUnusedNS: false },
          { removeUselessStrokeAndFill: false },
          { cleanupIDs: false },
          { removeComments: true },
          { removeEmptyAttrs: true },
          { removeEmptyText: true },
          { collapseGroups: true }
        ]
      })
    ])))
    .pipe(
      responsive(
        {
          // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
          '**/*.jpg': [
            {
              width: 200,
              rename: { suffix: `-s` }
            },
            {
              width: 500,
              rename: { suffix: `-m` }
            },
            {
              width: 630,
              rename: { suffix: `-l` }
            },
            {
              width: 200 * 2,
              rename: { suffix: `-s@2x` }
            },
            {
              width: 500 * 2,
              rename: { suffix: `-m@2x` }
            },
            {
              width: 630 * 2,
              rename: { suffix: `-l@2x` }
            },
            {
              withoutEnlargement: true,
            },
            {
              width: `200%`,
              rename: { suffix: `@2x` }
            }
          ],
          // Resize all PNG images to be retina ready
          '**/*.png': [
            {
              width: 200,
              rename: { suffix: `-s` },
            },
            {
              width: 500,
              rename: { suffix: `-m` }
            },
            {
              width: 630,
              rename: { suffix: `-l` }
            },
            {
              width: 200 * 2,
              rename: { suffix: `-s@2x` }
            },
            {
              width: 500 * 2,
              rename: { suffix: `-m@2x` }
            },
            {
              width: 630 * 2,
              rename: { suffix: `-l@2x` }
            },
            {
              withoutEnlargement: true,
            },
            {
              width: `200%`,
              rename: { suffix: `@2x` }
            }
          ]
        },
        {
          // Global configuration for all images
          // The output quality for JPEG, WebP and TIFF output formats
          quality: 80,
          //format: `webp`,  
          // Use progressive (interlace) scan for JPEG and PNG output
          progressive: true,
          format: `webp`,
          // Strip all metadata
          withMetadata: false,
          withoutEnlargement: false,
          skipOnEnlargement: true
        }
      )
    )
    .pipe(gulp.dest(paths.images.dist))
    .pipe(debug({
      "title": `Images`
    }))
    .pipe(browsersync.stream());
});
