"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import webpack from "webpack-stream";
import gulpif from "gulp-if";
import browsersync from "browser-sync";
import debug from "gulp-debug";
import yargs from "yargs";

const argv = yargs.argv,
  production = !!argv.production;

gulp.task(`scripts`, () => {
  return gulp
    .src(paths.scripts.src)
    .pipe(webpack(require(`../webpack.config.js`)))
    .pipe(gulpif(production, webpack(require(`../webpack.production.config.js`))))
    .pipe(gulp.dest(paths.scripts.dist))
    .pipe(debug({
      "title": `JS files`
    }))
    .pipe(browsersync.stream());
});
