"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import svg from "gulp-svg-sprite";
import debug from "gulp-debug";
import browsersync from "browser-sync";

gulp.task(`sprites`, () => {
  return gulp.src(paths.sprites.src)
    .pipe(svg({
      shape: {
        dest: `../sprites`
      },
      mode: {
        inline: true, // Prepare for inline embedding
        symbol: {
          sprite: `../sprite.symbol.svg`, // Sprite path and name
        }
      }
    }))
    .pipe(gulp.dest(paths.sprites.dist))
    .pipe(gulp.dest(paths.sprites.srcSvg))
    .pipe(debug({
      "title": `Sprites`
    }))
    .on(`end`, browsersync.reload);
});