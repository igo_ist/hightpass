"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import del from "del";

gulp.task(`cleanSprite`, () => {
  return del(`${paths.sprites.srcSvg}sprite.symbol.svg`);
});