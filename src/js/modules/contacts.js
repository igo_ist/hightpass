ymaps.ready(init);
function init() {
  let myPlacemark = new ymaps.Placemark([55.770801, 37.635754], {}, {
    iconLayout: `default#image`,
    iconImageHref: `assets/img/sprites/map-circle.svg`,
    iconImageSize: [12, 12],
    iconImageOffset: [-14, -50]
  });

  let myMap = new ymaps.Map(`map`, {
    center: [55.760221, 37.608561],
    zoom: 13.2,
    controls: []
  },{
    suppressMapOpenBlock: true
  });
  myMap.geoObjects.add(myPlacemark);
}