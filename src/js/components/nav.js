const burgerButton = document.querySelector(`.burger`);
const navList = document.querySelector(`.nav__list`);

function toogleNavigation() {
  burgerButton.classList.toggle(`open`);
  navList.classList.toggle(`open`);
}

burgerButton.addEventListener(`click`, toogleNavigation);

document
  .querySelectorAll(`.nav__item a[href^="#"]`)
  .forEach(trigger => {
    trigger.onclick = function (e) {
      e.preventDefault();
      toogleNavigation();
      let hash = this.getAttribute(`href`);
      let target = document.querySelector(hash);
      //let headerOffset = 100;
      let elementPosition = target.offsetTop;
      let offsetPosition = elementPosition/*  - headerOffset */;

      window.scrollTo({
        top: offsetPosition,
        behavior: `smooth`
      });
    };
  });