const smallDevice = window.matchMedia(`(min-width: 840px)`);
const form = document.querySelector(`.search`);
const button = document.querySelector(`.search__button`);
const input = document.querySelector(`.search__input`);

button.addEventListener(`click`, function (e) {
  if(!!input.value && smallDevice.matches) {
    form.submit();
  }
  if(form.classList.contains(`active`) || !!smallDevice.matches){
    e.preventDefault();
    form.classList.remove(`active`);
    input.classList.remove(`active`);
    input.blur();
    button.classList.remove(`cross`);
    button.style.fill = null;
    
  } else {
    e.preventDefault();
    form.classList.add(`active`);
    input.classList.add(`active`);
    button.classList.add(`cross`);
    button.style.fill = `transparent`;
  }
});
input.addEventListener(`keydown`, function (e) {
  if(!!input.value && e.keyCode === 13) {
    form.submit();
  }
});