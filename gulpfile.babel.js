"use strict";

import gulp from "gulp";
import path from "path";

const requireDir = require(`require-dir`);
const PATHS = {
  src: path.join(__dirname, `./src`),
  dist: path.join(__dirname, `./dist`),
  assets: `assets/`,
};
const paths = {
  pug: {
    src: `${PATHS.src}/pug/pages/*.pug`,
    dist: `${PATHS.dist}/`,
    watch: `./src/pug/**/*.pug`
  },
  styles: {
    src: `${PATHS.src}/${PATHS.assets}scss/main.{scss,sass}`,
    dist: `${PATHS.dist}/`,
    watch: `./src/${PATHS.assets}scss/**/*.{scss,sass}`,
  },
  scripts: {
    src: `${PATHS.src}/js/index.js`,
    dist: `${PATHS.dist}/`,
    watch: `./src/js/**/*.js`
  },
  images: {
    src: [
      `${PATHS.src}/${PATHS.assets}img/**/*.{jpg,jpeg,png,gif,tiff}`,
      `!${PATHS.src}/${PATHS.assets}img/favicon/*.{jpg,jpeg,png,gif,tiff}`
    ],
    dist: `${PATHS.dist}/${PATHS.assets}img/`,
    watch: `${PATHS.src}/${PATHS.assets}img/**/*.{jpg,jpeg,png,gif,svg}`
  },
  sprites: {
    src: `${PATHS.src}/${PATHS.assets}img/svg/*.svg`,
    srcSvg: `${PATHS.src}/${PATHS.assets}img/svg/`,
    dist: `${PATHS.dist}/${PATHS.assets}img/sprites/`,
    watch: `${PATHS.src}/${PATHS.assets}img/svg/*.svg`
  },
  fonts: {
    src: `${PATHS.src}/${PATHS.assets}fonts/**/*.{woff,woff2}`,
    dist: `${PATHS.dist}/${PATHS.assets}fonts/`,
    watch: `${PATHS.src}/${PATHS.assets}fonts/**/*.{woff,woff2}`
  },
  favicons: {
    src: `${PATHS.src}/img/favicon/*.{jpg,jpeg,png,gif,tiff}`,
    dist: `${PATHS.dist}/${PATHS.assets}static/favicons/`,
  },
  gzip: {
    src: `${PATHS.src}/.htaccess`,
    dist: `${PATHS.dist}/`
  }
};

requireDir(`./gulp-tasks/`);

export { paths, PATHS };

export const development = gulp.series(`clean`, `cleanSprite`, `sprites`,
  gulp.parallel([`images`, `fonts`, `favicons`, `scripts`]),
  gulp.parallel(`serve`));

export const prod = gulp.series(`clean`, `cleanSprite`, `sprites`,
  gulp.parallel([`images`, `fonts`, `favicons`, `gzip`, `scripts`]));

export default development;
